﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public float speed;
    public float distance;
    private Transform target;
    public Transform groundDetection;
    private bool movingRight = true;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        
    }

    // Update is called once per frame
    void Update()
    {
        // Move toward target
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distance);
        if (Vector2.Distance(transform.position, target.position) < 2 )
        {
            if (groundInfo.collider == true)
            {
                Debug.Log("GROUND");
                //Target on the left
                if (gameObject.transform.position.x > target.transform.position.x)
                    transform.eulerAngles = new Vector3(0, -180, 0);
                //Target on the right
                else
                    transform.eulerAngles = new Vector3(0, 0, 0);

                transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
            }
        }
        else
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
            if (groundInfo.collider == false)
            {
                if (movingRight == true)
                {
                    transform.eulerAngles = new Vector3(0, -180, 0);
                    movingRight = false;
                }
                else
                {
                    transform.eulerAngles = new Vector3(0, 0, 0);
                    movingRight = true;
                }
            }
        }
        
    }
}
