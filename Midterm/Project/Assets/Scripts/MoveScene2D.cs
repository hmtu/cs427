﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MoveScene2D : MonoBehaviour
{
    public GameObject door;
    void Start()
    {
        door.gameObject.SetActive(false);
    }
    void Update()
    {
        if(Score.scoreAmount == 5)
        {
            door.gameObject.SetActive(true);
        }
    }
}
