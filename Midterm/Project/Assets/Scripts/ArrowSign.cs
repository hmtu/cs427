﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ArrowSign : MonoBehaviour
{
    [SerializeField] private string newLevel;
    // Start is called before the first frame update
    // Update is called once per frame
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            SceneManager.LoadScene(newLevel);
        }
    }
}
