﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeButton : MonoBehaviour
{
    public MainMenu mainMenu;
    public GameObject panel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Resume()
    {
        Time.timeScale = 1;
        panel.gameObject.SetActive(!panel.gameObject.activeSelf);
        mainMenu.gameObject.SetActive(!mainMenu.gameObject.activeSelf);
    }
}
