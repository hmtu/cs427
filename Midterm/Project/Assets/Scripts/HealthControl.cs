﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthControl : MonoBehaviour
{
    public GameObject heart1, 
        heart2, 
        heart3, 
        heart4, 
        player, 
        gameOverText, 
        restartButton, 
        gameOverSound, 
        gameBackgroundSound;
    public static int health;
    // Start is called before the first frame update
    void Start()
    {
        health = 4;
        heart1.gameObject.SetActive(true);
        heart2.gameObject.SetActive(true);
        heart3.gameObject.SetActive(true);
        heart4.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (health > 4)
            health = 4;
        switch (health)
        {
            case 4:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                heart4.gameObject.SetActive(true);
                break;
            case 3:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(true);
                heart4.gameObject.SetActive(false);
                break;
            case 2:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(true);
                heart3.gameObject.SetActive(false);
                heart4.gameObject.SetActive(false);
                break;
            case 1:
                heart1.gameObject.SetActive(true);
                heart2.gameObject.SetActive(false);
                heart3.gameObject.SetActive(false);
                heart4.gameObject.SetActive(false);
                break;
            case 0:
                player.gameObject.SetActive(false);
                gameOverSound.gameObject.SetActive(true);
                gameBackgroundSound.gameObject.SetActive(false);
                gameOverText.gameObject.SetActive(true);
                restartButton.gameObject.SetActive(true);
                heart1.gameObject.SetActive(false);
                heart2.gameObject.SetActive(false);
                heart3.gameObject.SetActive(false);
                heart4.gameObject.SetActive(false);
                break;
        }
    }

    

}
