﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundAudio : MonoBehaviour
{
    private AudioSource audioSource;
    private float musicVolume = 0.5f;
    public Slider background;
    // Start is called before the first frame update

    void Awake()
    {
        background.value = PlayerPrefs.GetFloat("Slider value");
    }

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        audioSource.volume = musicVolume;
    }

    public void setVolume(float vol)
    {
        musicVolume = vol;
        PlayerPrefs.SetFloat("Slider value", vol);
    }
}
