﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public static AudioClip jumpSound, starSound, gameOverSound;
    static AudioSource audioSource;
    private float sfxVolume = 0.5f;
    public Slider sfxSlider;
    // Start is called before the first frame update
    void Awake()
    {
        sfxSlider.value = PlayerPrefs.GetFloat("SfxSlider value");
    }

    void Start()
    {
        jumpSound = Resources.Load<AudioClip>("jumpSound");
        starSound = Resources.Load<AudioClip>("StarSound");
        gameOverSound = Resources.Load<AudioClip>("gameOverSound");
        audioSource = GetComponent<AudioSource>();
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "jump":
                audioSource.PlayOneShot(jumpSound);
                break;
            case "star":
                audioSource.PlayOneShot(starSound);
                break;
            case "gameover":
                audioSource.PlayOneShot(gameOverSound);
                break;
        }
    }
    // Update is called once per frame
    void Update()
    {
        audioSource.volume = sfxVolume;
    }

    public void setVolume(float vol)
    {
        sfxVolume = vol;
        PlayerPrefs.SetFloat("SfxSlider value", vol);
    }
}
