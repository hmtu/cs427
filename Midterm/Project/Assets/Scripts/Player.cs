﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public float speed;
    public float jumpForce;
    private float moveInput;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    private bool facingRight = true;
    private bool isGrounded;
    private bool attacking;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask whatIsGround;
    private int extraJumps;
    public int extraJumpsValue;
    public GameObject gameOverText, restartButton, gameOverSound, gameBackgroundSound;
    private GameObject[] enemies;

    void Start()
    {
        Physics2D.IgnoreLayerCollision(9, 10, false);
        HealthControl.health = 4;
        gameOverText.SetActive(false);
        restartButton.SetActive(false);
        extraJumps = extraJumpsValue;
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
        moveInput = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);
        if(facingRight == false && moveInput > 0)
        {
            Flip();
        }
        else if(facingRight == true && moveInput < 0)
        {
            Flip();
        }
        
    }
    void Update()
    {
        if (isGrounded == true)
        {
            Debug.Log("GROUND");
            extraJumps = extraJumpsValue;
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) && extraJumps > 0)
        {
            SoundManager.PlaySound("jump");
            rb.velocity = Vector2.up * jumpForce;
            extraJumps--;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow) && extraJumps == 0 && isGrounded == true)
        {
            SoundManager.PlaySound("jump");
            rb.velocity = Vector2.up * jumpForce;
        }
    }
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 Scaler = transform.localScale;
        Scaler.x *= -1;
        transform.localScale = Scaler;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("Enemies") && attacking == false)
        {
            enemies = GameObject.FindGameObjectsWithTag("Enemies");
            Physics2D.IgnoreLayerCollision(9, 10, true);
            attacking = true;
            foreach(GameObject enemy in enemies)
            {
                enemy.gameObject.GetComponent<Follow>().enabled = false;
                enemy.gameObject.GetComponent<Patrol>().enabled = true;
            }
            StartCoroutine (Hit(enemies));
            
        }
        else if (col.gameObject.tag.Equals("Border"))
        {
            gameOverSound.gameObject.SetActive(true);
            gameBackgroundSound.gameObject.SetActive(false);
            gameOverText.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(true);
        }


    }

    //Set Player invisible for 2 second
    IEnumerator Hit(GameObject[] enemies)
    {
        HealthControl.health -= 1;
        gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, .5f);
        yield return new WaitForSeconds(3f);
        gameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
        foreach (GameObject enemy in enemies)
        {
            enemy.gameObject.GetComponent<Follow>().enabled = true;
            enemy.gameObject.GetComponent<Patrol>().enabled = false;
        }
        //enemy.GetComponent<Patrol>().enabled = false;
        //enemy.GetComponent<Follow>().enabled = true;
        Physics2D.IgnoreLayerCollision(9, 10, false);
        attacking = false;
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        SoundManager.PlaySound("star");
        Destroy(other.gameObject);
        Score.scoreAmount += 1;
    }

}
