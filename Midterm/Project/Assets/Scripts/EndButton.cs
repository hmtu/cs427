﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndButton : MonoBehaviour
{
    // Start is called before the first frame update
    public void BacktoMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
