﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionControl : MonoBehaviour
{
    public GameObject optionsMenu;
    public GameObject panel;

    // Update is called once per frame
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Running");
            panel.gameObject.SetActive(!panel.gameObject.activeSelf);
            optionsMenu.gameObject.SetActive(false);
        }
    }
}
