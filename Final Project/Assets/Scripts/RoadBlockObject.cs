﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadBlockObject : MonoBehaviour
{
    public GameObject roadBlock;
    GameObject car;
    private string car_name;
    public float turnSpeed = 80f;
    public bool canRotate = false;
    public bool select = false;
    void Update()
    {
        if (Input.GetKeyDown("r") && select == true)
        {
            StartRotation();
        }
        if (canRotate)
            roadBlock.transform.Rotate(Vector3.up * turnSpeed * Time.deltaTime);
        else if(Input.GetKeyDown(KeyCode.Delete) && select == true)
        {
            car_name = roadBlock.name.Remove(roadBlock.name.Length - 1);
            print("Car name: " + car_name);
            car = GameObject.Find(car_name);
            car.GetComponent<RoadBlock>().rb--;
            print("Car rb: " + car.GetComponent<RoadBlock>().rb);
            Destroy(gameObject);
        }
        if (Input.GetMouseButtonDown(0))
        {
            roadBlock.transform.Find("SelectIndicator").gameObject.SetActive(false);
            select = false;
            canRotate = false;
            enabled = false;
        }
    }
    //Your button Method
    public void StartRotation()
    {
        canRotate = !canRotate;
    }
}
