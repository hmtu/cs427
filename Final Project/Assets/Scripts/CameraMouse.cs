﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMouse : MonoBehaviour
{
    public float ZoomAmount = 0;
    public float MaxToClamp = 10f;
    public float ROTSpeed = 30f;
    public float speed = 30f;
    public float rotate_speed = 5f;
    private float X;
    private float Y;
    

    bool bDragging = false;
    Vector3 oldPos = new Vector3();
    Vector3 panOrigin = new Vector3();

    // Use this for initialization
    void Start()
    {

    }

    void Update()
    {
        Vector3 p = transform.position;
        ZoomAmount += Input.GetAxis("Mouse ScrollWheel");
        ZoomAmount = Mathf.Clamp(ZoomAmount, -MaxToClamp, MaxToClamp);
        var translate = Mathf.Min(Mathf.Abs(Input.GetAxis("Mouse ScrollWheel")), MaxToClamp - Mathf.Abs(ZoomAmount));
        gameObject.transform.Translate(0, 0, translate * ROTSpeed * Mathf.Sign(Input.GetAxis("Mouse ScrollWheel")));
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
        }
        if (Input.GetKey(KeyCode.S))
        {
            //transform.Translate(new Vector3(0, -speed * Time.deltaTime, 0));
        }
        if (Input.GetKey(KeyCode.W))
        {
            //transform.Translate(new Vector3(0, speed * Time.deltaTime, 0));
        }
        if (Input.GetMouseButton(2))
        {
            transform.Rotate(new Vector3(Input.GetAxis("Mouse Y") * rotate_speed, -Input.GetAxis("Mouse X") * rotate_speed, 0));
            X = transform.rotation.eulerAngles.x;
            Y = transform.rotation.eulerAngles.y;
            transform.rotation = Quaternion.Euler(X, Y, 0);
        }
    }

    
}
