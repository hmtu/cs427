﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadBlock : MonoBehaviour
{

    public GameObject roadBlock;
    private float carSpeed = 0f;
    public bool deployRoadBlock = false;
    public int rb;
    GameObject clone;
    // Start is called before the first frame update
    void Start()
    {
        rb = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);
        carSpeed = gameObject.GetComponent<CarEngine>().currentSpeed;
        gameObject.transform.Find("SelectIndicator").gameObject.SetActive(true);
        if (Input.GetMouseButtonDown(0) && carSpeed < 1)
        {
            print("Roadblock Deploying");
            deployRoadBlock = true;
            if (rb < 3)
            {
                spawnRoadBlock(mousePos);
            }
            else
                print("RoadBlock full");
            //or for tandom rotarion use Quaternion.LookRotation(Random.insideUnitSphere)
        }
        if (Input.GetMouseButtonDown(1))
        {
            print("Roadblock Cancel");
            gameObject.transform.Find("SelectIndicator").gameObject.SetActive(false);
            gameObject.GetComponent<CarController>().roadBlock = false;
            deployRoadBlock = false;
            enabled = false;
        }
        if (Input.GetKeyDown("r"))
        {
            print("Roadblock Cancel");
            gameObject.transform.Find("SelectIndicator").gameObject.SetActive(true);
            gameObject.GetComponent<CarController>().enabled = true;
            gameObject.GetComponent<CarController>().select = true;
            gameObject.GetComponentInChildren<LightManager>().enabled = true;
            gameObject.GetComponentInChildren<LightManager>().select = true;
            gameObject.GetComponent<CarController>().roadBlock = false;
            gameObject.GetComponent<CarController>().rClick = true;
            deployRoadBlock = false;
            enabled = false;
        }
    }
    void spawnRoadBlock(Vector3 mousePos)
    {
        Vector3 wordPos;
        Ray ray = Camera.main.ScreenPointToRay(mousePos);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f))
        {
            wordPos = hit.point;
            wordPos.y = 1.4f;
        }
        else
        {
            wordPos = Camera.main.ScreenToWorldPoint(mousePos);
        }
        clone = Instantiate(roadBlock, wordPos, Quaternion.Euler(0, 0, 0));
        clone.name = gameObject.name + rb;
        rb++;
    }
}
