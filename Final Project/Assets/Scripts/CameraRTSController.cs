﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRTSController : MonoBehaviour
{
    public Camera cam;
    public float panSpeed = 20f;
    public float panBorderThickness = 10f;
    public Vector2 panLimit;
    public float scrollSpeed;
    public float rotate_speed = 5f;
    public float minPanY = 20f;
    public float maxPanY = 120f;
    public float minPanX;
    public float maxPanX;
    public float minPanZ;
    public float maxPanZ;
    private float X;
    private float Y;
    private Vector2 MouseAxis
    {
        get { return new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")); }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        Quaternion rot = transform.rotation;
        Vector3 fw = transform.forward;
        fw.y = 0;
        fw.Normalize();
        if(fw != Vector3.zero)
        {
            if (Input.GetKey("w"))
            {
                // || Input.mousePosition.y >= Screen.height - panBorderThickness
                //pos.z += panSpeed * Time.deltaTime;      
                pos += fw * Time.deltaTime * panSpeed;
            }

            if (Input.GetKey("s"))
            {
                // || Input.mousePosition.y <=  panBorderThickness
                //pos.z -= panSpeed * Time.deltaTime;
                pos += -fw * Time.deltaTime * panSpeed;
            }

            if (Input.GetKey("d"))
            {
                // || Input.mousePosition.x >= Screen.width - panBorderThickness
                //pos.x += panSpeed * Time.deltaTime;
                pos += transform.right * Time.deltaTime * panSpeed;
            }

            if (Input.GetKey("a"))
            {
                // || Input.mousePosition.x <= panBorderThickness
                //pos.x -= panSpeed * Time.deltaTime;
                pos += -transform.right * Time.deltaTime * panSpeed;
            }
        }
        

        
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        pos.y -= scroll * scrollSpeed * 100f * Time.deltaTime;

        pos.x = Mathf.Clamp(pos.x, minPanX, maxPanX);
        pos.y = Mathf.Clamp(pos.y, minPanY, maxPanY);
        pos.z = Mathf.Clamp(pos.z, minPanZ, maxPanZ);

        //if (Input.GetMouseButton(2))
        //{
        //    transform.Rotate(new Vector3(Input.GetAxis("Mouse Y") * rotate_speed, -Input.GetAxis("Mouse X") * rotate_speed, 0));
        //   X = transform.rotation.eulerAngles.x;
        //    Y = transform.rotation.eulerAngles.y;
        //    rot = Quaternion.Euler(X, Y, 0);
        //}
        //transform.rotation = rot;
        if (Input.GetMouseButton(2))
        {
            transform.Rotate(new Vector3(Input.GetAxis("Mouse Y") * rotate_speed, -Input.GetAxis("Mouse X") * rotate_speed, 0));
            X = transform.rotation.eulerAngles.x;
            Y = transform.rotation.eulerAngles.y;
            transform.rotation = Quaternion.Euler(X, Y, 0);
        }
        transform.position = pos;
    }
}
