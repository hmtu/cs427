﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Common;

namespace Lean.Gui
{
    public class Spawner : MonoBehaviour
    {
        public Transform[] spawnLocation;
        public GameObject[] whatToSpawnPrefab;
        private GameObject clone;
        public GameObject[] whatToSpawnClone;
        public GameObject button;
        private AudioSource dispatch;
        private Slider slider;
        private GameObject car_clone;
        private int numOfClick = 0;
        private int spawnCar = 0;
        private float mode = 0;
        private string button_name;
        void Start()
        {
            dispatch = GetComponent<AudioSource>();
            //SpawnCar();
            //StartCoroutine(SpawnCar());
        }

        private void Update()
        {

        }

        public void ButtonClick()
        {
            if (numOfClick == 0)
            {
                numOfClick++;
                StartCoroutine(SpawnCar());
            }
        }

        IEnumerator SpawnCar()
        {
            button_name = button.GetComponentInChildren<Text>().text;
            button.GetComponent<Image>().color = new Color32(176, 0, 32, 255);
            gameObject.GetComponent<LeanButton>().enabled = false;
            button.GetComponentInChildren<Text>().text = "LOADING...";
            yield return new WaitForSeconds(5);
            whatToSpawnClone[0] = Instantiate(whatToSpawnPrefab[0], spawnLocation[0].transform.position, Quaternion.Euler(0, 180, 0)) as GameObject;
            car_clone = whatToSpawnClone[0];
            print("Slider: " + Resources.FindObjectsOfTypeAll<Slider>());
            print("Path name: " + GameObject.Find("Path").name);
            if (Resources.FindObjectsOfTypeAll<Slider>().Length > 0)
            {
                slider = Resources.FindObjectsOfTypeAll<Slider>()[0];
                print("Slider name: " + slider.name);
                car_clone.GetComponentInChildren<LightManager>().sirenSlider = slider;
                car_clone.GetComponentInChildren<LightManager>().sirenVolume = PlayerPrefs.GetFloat("SfxSlider value");
                //car_clone.GetComponent<CarEngine>().enabled = true;
                car_clone.GetComponent<CarEngine>().maxMotorTorque = 100f;
                car_clone.GetComponent<CarEngine>().maxSpeed = 80f;
                car_clone.GetComponent<CarEngine>().path = GameObject.Find("Police Path").transform;
            }
            numOfClick--;
            dispatch.volume = PlayerPrefs.GetFloat("SfxSlider value");
            dispatch.Play();
            button.GetComponent<Image>().color = new Color32(53, 118, 178, 255);
            button.GetComponentInChildren<Text>().text = button_name;
            gameObject.GetComponent<LeanButton>().enabled = true;
        }
    }
}

