﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resume : MonoBehaviour
{
    public MainMenu mainMenu;
    public GameObject panel;
    public GameObject camera;
    public GameObject RTSMenu;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public void ResumeButton()
    {
        Time.timeScale = 1;
        camera.GetComponent<UnitSelectionComponent>().enabled = true;
        panel.gameObject.SetActive(!panel.gameObject.activeSelf);
        mainMenu.gameObject.SetActive(!mainMenu.gameObject.activeSelf);
        RTSMenu.SetActive(true);
        this.transform.GetComponentInParent<MenuControl>().isPaused = false;
    }
}
