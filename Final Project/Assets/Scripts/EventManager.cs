﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public bool complete = false;
    public GameObject completeCanvas;
    public GameObject playerMenu;
    public int numberOfSuspects;
    public int numberOfArrests = 0;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        print("Arrest num: " + numberOfArrests);
        if (complete == true && numberOfArrests == numberOfSuspects)
        {
            print("Mission completed!");
            Time.timeScale = 0;
            playerMenu.SetActive(false);
            completeCanvas.SetActive(true);
        } 
    }

}
