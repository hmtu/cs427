﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarEngine : MonoBehaviour
{
    public Transform path;
    public float maxSteerAngle = 45f;
    public float turnSpeed = 5f;
    public WheelCollider WheelFL; //Front Left
    public WheelCollider WheelFR; //Front Right
    public WheelCollider WheelRL; //Front Left
    public WheelCollider WheelRR; //Front Right
    public bool isBraking = false;
    public bool isStartPatrol = false;
    public float currentSpeed;
    public float maxSpeed = 150f;
    public float maxMotorTorque = 150f;
    public float maxBrakeTorque = 300f;

    private List<Transform> nodes;
    private int currentNode;
    private float lastDist = 100f;
    private bool avoiding = false;
    private float targetSteerAngle = 0;

    public Vector3 centerOfMass;
    public Texture2D textureNormal;
    public Texture2D textureBraking;
    public Renderer carRenderer;

    [Header("Sensors")]
    public float sensorLength = 5f;
    public Vector3 frontSensorPosition = new Vector3(0,0,3f);
    public float frontSideSensorPosition = 0.2f;
    public float frontSensorAngle = 30f;



    // Start is called before the first frame update
    private void Start()
    {
        GetComponent<Rigidbody>().centerOfMass = centerOfMass;
        Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();
        nodes = new List<Transform>();
        isStartPatrol = true;
        for (int i = 0; i < pathTransforms.Length; i++)
        {
            if (pathTransforms[i] != path.transform)
            {
                nodes.Add(pathTransforms[i]);
            }
        }
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        Sensors();
        ApplySteer();
        Drive();
        CheckWayPointDistance();
        Braking();
        LerpToSteerAngle();
    }

    private void Sensors()
    {
        RaycastHit hit;
        Vector3 sensorStartPos = transform.position;
        sensorStartPos += transform.forward * frontSensorPosition.z;
        sensorStartPos += transform.up * frontSensorPosition.y;
        float avoidMultiplier = 0;
        avoiding = false;


        //Front Right sensor
        sensorStartPos += transform.right * frontSideSensorPosition;
        
        if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength))
        {
            if (!hit.collider.CompareTag("Terrain"))
            {
                Debug.DrawLine(sensorStartPos, hit.point, Color.blue);
                avoiding = true;
                avoidMultiplier -= 1f;
            }
        }
        //Front right angle sensor

        else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength))
        {
            if (!hit.collider.CompareTag("Terrain"))
            {
                Debug.DrawLine(sensorStartPos, hit.point, Color.white);
                avoiding = true;
                avoidMultiplier -= 0.5f;
            }
        }
        
        //Front Left sensor
        sensorStartPos -= transform.right * frontSideSensorPosition * 2;
        if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength))
        {
            if (!hit.collider.CompareTag("Terrain"))
            {
                Debug.DrawLine(sensorStartPos, hit.point, Color.yellow);
                avoiding = true;
                avoidMultiplier += 1f;
            }
        }
        
        //Front left angle sensor
        else if (Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength))
        {
            if (!hit.collider.CompareTag("Terrain"))
            {
                Debug.DrawLine(sensorStartPos, hit.point, Color.black);
                avoiding = true;
                avoidMultiplier += 0.5f;
            }
        }

        //Front Center Sensor
        if(avoidMultiplier == 0)
        {
            if (Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength))
            {

                if (!hit.collider.CompareTag("Terrain"))
                {
                    Debug.DrawLine(sensorStartPos, hit.point, Color.red);
                    avoiding = true;
                    if (hit.normal.x < 0)
                        avoidMultiplier = -1;
                    else
                        avoidMultiplier = 1;
                }
            }
        }
            
        if (avoiding)
        {
            targetSteerAngle = maxSteerAngle * avoidMultiplier;
        }

    }

    private void ApplySteer()
    {
        if (avoiding) return;
        Vector3 relativeVector = transform.InverseTransformPoint(nodes[currentNode].position);
        float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;
        targetSteerAngle = newSteer;
        //WheelFL.steerAngle = newSteer;
        //WheelFR.steerAngle = newSteer;
    }

    private void Drive()
    {
        currentSpeed = 2 * Mathf.PI * WheelFL.radius * WheelFL.rpm * 60 / 1000;
        if (currentSpeed < maxSpeed && !isBraking)
        {
            WheelFL.motorTorque = maxMotorTorque;
            WheelFR.motorTorque = maxMotorTorque;
        }
        else
        {
            WheelFL.motorTorque = 0;
            WheelFR.motorTorque = 0;
        }

    }
    private void CheckWayPointDistance()
    {
        if ((Vector3.Distance(transform.position, nodes[currentNode].position) < 10f) && isStartPatrol == false)
        {
            if (currentNode == nodes.Count - 1)
            {
                currentNode = 0;
            }
            else
                currentNode++;
            print("Current node:" + currentNode);
        }
        else if(isStartPatrol == true)
        {
            currentNode = findClosestCheckpoint();
            print("Current node:" + currentNode);
        }
    }

    private int findClosestCheckpoint()
    {
        if (nodes.Count == 0) return -1;
        int closest = 0;
        lastDist = Vector3.Distance(transform.position, nodes[0].transform.position);
        for (int i = 1; i < nodes.Count; i++)
        {
            float thisDist = Vector3.Distance(transform.position, nodes[i].position);
            if (lastDist > thisDist && i != currentNode)
            {
                closest = i;
                lastDist = thisDist;
            }
        }
        print("Closest: " + closest);
        isStartPatrol = false;
        return closest;
    }


    private void Braking()
    {
        if (isBraking)
        {
            carRenderer.material.mainTexture = textureBraking;
            WheelRL.brakeTorque = maxBrakeTorque;
            WheelRR.brakeTorque = maxBrakeTorque;
        }
        else
        {
            carRenderer.material.mainTexture = textureNormal;
            WheelRL.brakeTorque = 0;
            WheelRR.brakeTorque = 0;
        }
    }

    private void LerpToSteerAngle()
    {
        WheelFL.steerAngle = Mathf.Lerp(WheelFL.steerAngle, targetSteerAngle, Time.deltaTime * turnSpeed);
        WheelFR.steerAngle = Mathf.Lerp(WheelFR.steerAngle, targetSteerAngle, Time.deltaTime * turnSpeed);
    }
}