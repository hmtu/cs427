﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Suspect : MonoBehaviour
{
    private float speed;
    private Rigidbody suspectRigidbody;
    private bool isChased = false;
    public Transform objectTransfom;
    public Camera camera;
    public GameObject arrestProgress;
    public GameObject eventManager;
    public bool isArrest = false;
    private AudioSource arrestedReport;
    public GameObject gameOver;

    private float noMovementThreshold = 0.0001f;
    private const int noMovementFrames = 3;
    Vector3[] previousLocations = new Vector3[noMovementFrames];
    public bool isMoving;
    private int collisionCount = 0;

    public bool IsMoving
    {
        get { return isMoving; }
    }
    private void Start()
    {
        arrestedReport = GetComponent<AudioSource>();
    }

    void Awake()
    {
        //For good measure, set the previous locations
        for (int i = 0; i < previousLocations.Length; i++)
        {
            previousLocations[i] = Vector3.zero;
        }
    }

    private void Update()
    {
        suspectRigidbody = GetComponent<Rigidbody>();
        speed = gameObject.GetComponent<SuspectEngine>().currentSpeed;
        print("isChased :" + isChased);
        print("Collision count: " + collisionCount + " of " + gameObject.name);
        if(collisionCount == 3 && isChased == true)
        {
            print("Police got me");      
            //suspectRigidbody.constraints = RigidbodyConstraints.FreezePosition;
            eventManager.GetComponent<EventManager>().numberOfArrests++;
            gameObject.GetComponent<SuspectEngine>().isBraking = true;
            arrestProgress.SetActive(true);
            isArrest = true;
            arrestedReport.volume = PlayerPrefs.GetFloat("SfxSlider value");
            arrestedReport.Play();
            enabled = false;
        }

        //Store the newest vector at the end of the list of vectors
        for (int i = 0; i < previousLocations.Length - 1; i++)
        {
            previousLocations[i] = previousLocations[i + 1];
        }
        previousLocations[previousLocations.Length - 1] = objectTransfom.position;

       
        for (int i = 0; i < previousLocations.Length - 1; i++)
        {
            if (Vector3.Distance(previousLocations[i], previousLocations[i + 1]) >= noMovementThreshold)
            {
                //The minimum movement has been detected between frames
                print("Moving now");
                isMoving = true;
                break;
            }
            else
            {
                print("Not moving!!!");
                //gameObject.GetComponent<SuspectEngine>().isBraking = true;
                isMoving = false;
            }
        }


    }
    void Victory()
    {
        GameObject.Find("EventManager").GetComponent<EventManager>().complete = true;
    }
    private void OnCollisionEnter(Collision collision)
    {
        //speed = gameObject.GetComponent<SuspectEngine>().currentSpeed;
        print("Suspect speed: " + speed);
        //|| (collision.gameObject.tag == "Road Block" && speed < 2)
        if ((collision.gameObject.tag == "Police"))
        {
            //gameObject.GetComponent<SuspectEngine>().isBraking = true;
            //print("Police got me");
            print("Collision");
            collisionCount++;
            isChased = true;
        }
        else
        {
            isChased = false;
        }
        if ((collision.gameObject.tag == "Exit"))
        {
            Destroy(gameObject);
            print("escape");
            gameOver.SetActive(true);
        }
    }
}
