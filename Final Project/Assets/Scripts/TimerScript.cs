﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
    Image timeBar;
    public float maxTime = 3f;
    public float numberOfSuspects;
    float timeLeft;
    //private int arrests = 0;
    public GameObject levelComplete;
    // Start is called before the first frame update
    void Start()
    {
        timeBar = GetComponent<Image>();
        timeLeft = maxTime;
    }

    // Update is called once per frame
    void Update()
    {
        if(timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
            timeBar.fillAmount = timeLeft / maxTime;
        }
        else
        {
           gameObject.SetActive(false);
           timeLeft += Time.deltaTime * numberOfSuspects;
           GameObject.Find("EventManager").GetComponent<EventManager>().complete = true;
        }
    }
}
