﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CarController : MonoBehaviour
{
    public LayerMask WhatCanBeClikedOn;
    public GameObject indicator;
    public bool arrive = false;
    public bool patrol = false;
    private NavMeshAgent myAgent;
    public bool select = false;
    public bool roadBlock = false;
    Rigidbody carRigidbody;
    public bool rClick = false;

    void Start()
    {
        //indicator.gameObject.SetActive(true);
        myAgent = GetComponent<NavMeshAgent>();
        //gameObject.GetComponent<NavMeshAgent>().enabled = true;
        carRigidbody = gameObject.GetComponent<Rigidbody>();
    }
    void Update()
    {
        //indicator.gameObject.SetActive(true);
        if (Input.GetMouseButton(1))
        {
            if(select == true)
            {
                myAgent.enabled = true;
                gameObject.GetComponent<NavMeshObstacle>().enabled = false;
                carRigidbody.constraints =
                RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationZ |
                RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotationX |
                RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationY;
                patrol = false;
                Ray myRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitInfo;

                if (Physics.Raycast(myRay, out hitInfo, 10000, WhatCanBeClikedOn))
                {
                    SetTargetVector(hitInfo.point);
                }
            }
        }

        if (Input.GetMouseButton(0) && rClick == true)
        {
            gameObject.transform.Find("SelectIndicator").gameObject.SetActive(false);
            //gameObject.GetComponentInChildren<LightManager>().enabled = false;
            gameObject.GetComponentInChildren<LightManager>().select = false;
            if (!myAgent.pathPending && myAgent.enabled == true && patrol == false)
            {
                if (myAgent.remainingDistance <= myAgent.stoppingDistance)
                {
                    if (!myAgent.hasPath || myAgent.velocity.sqrMagnitude == 0f)
                    {
                        arrive = true;
                        myAgent.enabled = false;
                        gameObject.GetComponent<NavMeshObstacle>().enabled = true;
                        gameObject.GetComponent<CarEngine>().isBraking = true;
                        enabled = false;
                    }
                }
            }
            rClick = false;
            select = false;
        }

            //Disable nav mesh if car no longer selected
        if (select == false)
        {
            //print("Patrol: " + patrol);
            if (!myAgent.pathPending && myAgent.enabled == true && patrol == false)
            {
                if (myAgent.remainingDistance <= myAgent.stoppingDistance)
                {
                    if (!myAgent.hasPath || myAgent.velocity.sqrMagnitude == 0f)
                    {
                        arrive = true;
                        myAgent.enabled = false;
                        gameObject.GetComponent<NavMeshObstacle>().enabled = true;
                        enabled = false;
                    }
                }
            }
        }

        if(select == true)
        {
            if (Input.GetKeyDown("p"))
            {
                patrol = true;
                myAgent.enabled = false;
                gameObject.GetComponent<NavMeshObstacle>().enabled = true;
                carRigidbody.constraints = RigidbodyConstraints.None;
                gameObject.GetComponent<CarEngine>().enabled = true;
                gameObject.GetComponent<CarEngine>().isBraking = false;
                gameObject.GetComponent<CarEngine>().maxMotorTorque = 100;
                gameObject.GetComponent<CarEngine>().maxSpeed = 80f;
            }
            if (Input.GetKeyDown("r"))
            {
                print("RoadBlock status: " + roadBlock);
                if(roadBlock == false)
                {
                    roadBlock = true;
                    gameObject.GetComponent<RoadBlock>().enabled = true;       
                }
                else
                {
                    roadBlock = false;
                    gameObject.GetComponent<RoadBlock>().enabled = false;
                }
            }
            if (Input.GetKeyDown("b"))
            {
                if(gameObject.GetComponent<CarEngine>().isBraking == true)
                    gameObject.GetComponent<CarEngine>().isBraking = false;
                else
                    gameObject.GetComponent<CarEngine>().isBraking = true;
            }
        }
    }
    public void SetTargetVector(Vector3 target)
    {
        myAgent.SetDestination(target);
        gameObject.GetComponent<CarEngine>().isBraking = true;
    }

    private void OnMouseDown()
    {
        enabled = true;
    }
}
