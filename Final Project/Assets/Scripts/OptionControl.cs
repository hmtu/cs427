﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionControl : MonoBehaviour
{
    public GameObject optionsMenu;
    public GameObject panel;
    public GameObject camera;
    public GameObject RTSMenu;
    public bool isPause = false;

    void Awake()
    {
        camera.GetComponent<UnitSelectionComponent>().enabled = false;
    }


    // Update is called once per frame
    public void Update()
    {
        camera.GetComponent<UnitSelectionComponent>().enabled = false;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPause = true ;
            Debug.Log("Running");
            panel.gameObject.SetActive(!panel.gameObject.activeSelf);
            this.transform.GetComponentInParent<MenuControl>().isPaused = true;
            RTSMenu.SetActive(false);
            optionsMenu.gameObject.SetActive(false);
        }
    }
    //public void Back()
    //{
    //    isPause = true;
    //    Debug.Log("Running");
    //    panel.gameObject.SetActive(!panel.gameObject.activeSelf);
    //    this.transform.GetComponentInParent<MenuControl>().isPaused = true;
    //    RTSMenu.SetActive(false);
    //    optionsMenu.gameObject.SetActive(false);
    //}
}
