﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public static AudioClip siren, siren2, horn;
    static AudioSource audioSource;
    public float sfxVolume = 0.5f;
    public Slider sfxSlider;
    // Start is called before the first frame update
    void Awake()
    {
        sfxSlider.value = PlayerPrefs.GetFloat("SfxSlider value");
    }

    void Start()
    {
        siren = Resources.Load<AudioClip>("Siren");
        siren2 = Resources.Load<AudioClip>("Siren2");
        horn = Resources.Load<AudioClip>("Horn");
        audioSource = GetComponent<AudioSource>();
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "siren":
                audioSource.loop = true;
                audioSource.clip = siren;
                audioSource.Play();
                //audioSource.PlayOneShot(siren);
                break;
            case "siren2":
                audioSource.loop = true;
                audioSource.clip = siren2;
                audioSource.Play();
                //audioSource.PlayOneShot(siren2);
                break;
            case "horn":
                audioSource.loop = true;
                audioSource.clip = horn;
                audioSource.Play();
                //audioSource.PlayOneShot(horn);
                //audioSource.PlayOneShot(horn);
                break;
        }
    }
    // Update is called once per frame
    void Update()
    {
        audioSource.volume = sfxVolume;
    }

    public static void Stop()
    {
        audioSource.Stop();
    }

    public void setVolume(float vol)
    {
        sfxVolume = vol;
        PlayerPrefs.SetFloat("SfxSlider value", vol);
    }
}
