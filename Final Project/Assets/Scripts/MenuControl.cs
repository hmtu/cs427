﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuControl : MonoBehaviour
{
    // Start is called before the first frame update
    public MainMenu mainMenu;
    public GameObject panel;
    public GameObject camera;
    public GameObject optionMenu;
    public GameObject RTSMenu;
    public bool isPaused = false;
    // Update is called once per frame
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            print("Option pause: " + optionMenu.GetComponent<OptionControl>().isPause);
            print("Menu pause: " + isPaused);
            //Pause game when go to menu
            if (isPaused == false)
            {
                print("pause :1");
                Time.timeScale = 0;
                isPaused = true;
                camera.GetComponent<UnitSelectionComponent>().enabled = false;
                RTSMenu.SetActive(false);
            }
            else
            {
                print("pause :2");
                Time.timeScale = 1;
                isPaused = false;
                camera.GetComponent<UnitSelectionComponent>().enabled = true;
                RTSMenu.SetActive(true);
            }
            Debug.Log("Running");
            panel.gameObject.SetActive(!panel.gameObject.activeSelf);
            mainMenu.gameObject.SetActive(!mainMenu.gameObject.activeSelf);
        }
    }
}
